/*
 * button.h
 *
 *  Created on: Aug 3, 2020
 *      Author: Mateusz Salamon
 *      Modyfication: Jarosław Gutorski
 */

#ifndef INC_BUTTON_H_
#define INC_BUTTON_H_


uint32_t ControlTimer;
//uint32_t TimerReset;

typedef enum {
	BUTTON_IDLE = 0,
	BUTTON_DEBOUNCE, // 1
	BUTTON_PRESSED, // 2
	BUTTON_REPEAT, // 3
	BUTTON_RELEASE
} td_button_state;

typedef struct button_struct {
	td_button_state State;
	GPIO_TypeDef *GpioPort;
	uint16_t GpioPin;
	uint32_t LastTick;
	uint32_t Timer;
	uint32_t TimerIdle;
	uint32_t TimerDebounce;
	uint32_t TimerPressed;
	uint32_t TimerRepeat;
	void (*ButtonPress)(void);
	void (*ButtonLongPress)(void);
	void (*ButtonRepeatPress)(void);
	void (*ButtonRelease)(void);
} td_button;

void ButtonInitKey(td_button* Key, GPIO_TypeDef* GpioPort, uint16_t GpioPin, uint32_t TimerIdle,
					uint32_t TimerDebounce, uint32_t TimerPressed, uint32_t TimerRepeat);

void ButtonRegisterPressCallback(td_button* Key, void* Callback);
void ButtonRegisterLongPressCallback(td_button* Key, void* Callback);
void ButtonRegisterRepeatPressCallback(td_button* Key, void* Callback);
void ButtonRegisterReleaseCallback(td_button * Key, void * Callback);

void ButtonTask(td_button* Key);

_Bool ButtonPressed(td_button * key);
_Bool TimerOk(td_button * key);
void SetNewStatus(td_button * key, td_button_state new_state, uint32_t new_timer);

#endif /* INC_BUTTON_H_ */
