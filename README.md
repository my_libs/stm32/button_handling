# button_handling

Obsługa przycisku wg Kurs STM32 Mateusz Salamon


Biblioteka do obsługi przycisku (button switch), nieblokująca, z debouncingiem.
Biblioteka umożliwia wykonanie funkcji callback dla każdej akcji:

- wciśnięcie przycisku
- długie wciśnięcie przycisku
- ciągłe trzymanie, funkcja powtarzania
- zwolnienie (puszczenie) przycisku

Do funkcji inicjującej należy przekazać:

- zdres zmiennej strukturalnej przycisku
- adres portu
- numer portu 
- interwał czasu co ile będzie badany stan portu w stanie bezczynnoći (IDLE)
- czas debouncingu
- czas długiego przystymania przycisku
- czas powtarzania wywołania callbacku przy ciągłym trzymaniu przycisku (gdy minie czas długiego wciśnięcia)

`ButtonInitKey(&adres_zmiennej_strukturalnej, adres_portu, numer_pinu, interwał_badania_stanu_pinu, czas_debouncing, czas_długiego_wciśnięcia, interwał_powtarzania_callbacku);`

W głównej pętli należy umieścić funkcję `ButtonTask(&adres_zmiennej_strukruralnej)`

Do rejestracji callbacków służą funkcje:

- `ButtonRegisterPressCallback(&adres_zmiennej_strukturalnej, adres_wykonywanej_funkcji)`
- `ButtonRegisterLongPressCallback(&adres_zmiennej_strukturalnej, adres_wykonywanej_funkcji)`
- `ButtonRegisterRepeatPressCallback(&adres_zmiennej_strukturalnej, adres_wykonywanej_funkcji)`
- `ButtonRegisterReleaseCallback(&adres_zmiennej_strukturalnej, adres_wykonywanej_funkcji)`

