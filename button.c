/*
 * button.c
 *
 *  Created on: Aug 3, 2020
 *      Author: Mateusz Salamon
 *      Modyfication: Jarosław Gutorski
 */
#include <stdbool.h>

#include "main.h"
#include "button.h"


void ButtonInitKey(td_button *Key, GPIO_TypeDef *GpioPort, uint16_t GpioPin,
		uint32_t TimerIdle, uint32_t TimerDebounce, uint32_t TimerPressed,
		uint32_t TimerRepeat) {
	Key->GpioPort = GpioPort;
	Key->GpioPin = GpioPin;
	Key->State = BUTTON_IDLE;
	Key->Timer = 0;
	Key->TimerIdle = TimerIdle;
	Key->TimerDebounce = TimerDebounce;
	Key->TimerPressed = TimerPressed;
	Key->TimerRepeat = TimerRepeat;
}

void ButtonRegisterPressCallback(td_button *Key, void *Callback) {
	Key->ButtonPress = Callback;
}

void ButtonRegisterLongPressCallback(td_button *Key, void *Callback) {
	Key->ButtonLongPress = Callback;
}

void ButtonRegisterRepeatPressCallback(td_button *Key, void *Callback) {
	Key->ButtonRepeatPress = Callback;
}

void ButtonRegisterReleaseCallback(td_button *Key, void *Callback) {
	Key->ButtonRelease = Callback;
}

void ButtonIdleRoutine(td_button *Key) {
	if (TimerOk(Key)) {
		if (ButtonPressed(Key)) {
			SetNewStatus(Key, BUTTON_DEBOUNCE, Key->TimerDebounce);
		}
	}
}

void ButtonDebounceRoutine(td_button *Key) {
	if (ButtonPressed(Key)) {
		if (TimerOk(Key)) {
			if (Key->ButtonPress != NULL) {
				Key->ButtonPress();
			}
			SetNewStatus(Key, BUTTON_PRESSED, Key->TimerPressed);
		}
	}
	else {
		SetNewStatus(Key, BUTTON_IDLE, Key->TimerIdle);
	}
}

void ButtonPressedRoutine(td_button *Key) {
	if (ButtonPressed(Key)) {
		if (TimerOk(Key)) {
			if (Key->ButtonLongPress != NULL) {
				Key->ButtonLongPress();
			}
			SetNewStatus(Key, BUTTON_REPEAT, Key->TimerRepeat);
		}
	}
	else {
		SetNewStatus(Key, BUTTON_RELEASE, Key->TimerIdle);
	}
}

void ButtonRepeatRoutine(td_button *Key) {
	if (ButtonPressed(Key)) {
		if (TimerOk(Key)) {
			if (Key->ButtonRepeatPress != NULL) {
				Key->ButtonRepeatPress();
			}
			Key->LastTick = HAL_GetTick();
		}
	}
	else {
		SetNewStatus(Key, BUTTON_RELEASE, Key->TimerIdle);
	}
}

void ButtonReleaseRoutine(td_button *Key) {
	if (Key->ButtonRelease != NULL) {
		Key->ButtonRelease();
	}
	SetNewStatus(Key, BUTTON_IDLE, Key->TimerIdle);
}

void ButtonTask(td_button *Key) {
	switch (Key->State) {
	case BUTTON_IDLE:
		ButtonIdleRoutine(Key);
		break;

	case BUTTON_DEBOUNCE:
		ButtonDebounceRoutine(Key);
		break;

	case BUTTON_PRESSED:
		ButtonPressedRoutine(Key);
		break;

	case BUTTON_REPEAT:
		ButtonRepeatRoutine(Key);
		break;

	case BUTTON_RELEASE:
		ButtonReleaseRoutine(Key);

	default:
		break;
	}
}

_Bool ButtonPressed(td_button *key) {
	return GPIO_PIN_RESET == HAL_GPIO_ReadPin(key->GpioPort, key->GpioPin);
}

_Bool TimerOk(td_button *key) {
	return (HAL_GetTick() - key->LastTick) > key->Timer;
}

void SetNewStatus(td_button *key, td_button_state new_state, uint32_t new_timer) {
	key->State = new_state;
	key->Timer = new_timer;
	key->LastTick = HAL_GetTick();
}
